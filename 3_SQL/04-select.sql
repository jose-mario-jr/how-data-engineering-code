CREATE TABLE public."Billboard" (
    "date" date NULL,
    "rank" int4 NULL,
    song varchar(300) NULL,
    artist varchar(300) NULL,
    "last-week" int4 NULL,
    "peak-rank" int4 NULL,
    "weeks-on-board" int4 NULL
);

select count(*) 
from public."Billboard"
limit 100

select * 
from public."Billboard"
limit 100

SELECT
    "date",
    "rank",
    song,
    artist,
    "last-week", 
    "peak-rank",
    "weeks-on-board"
FROM
    public."Billboard"
LIMIT 100

SELECT
    t1."date",
    t1."rank",
    t1.song,
    t1.artist,
    t1."last-week", 
    t1."peak-rank",
    t1."weeks-on-board"
FROM
    public."Billboard" AS t1
LIMIT 100

-- poor sql
SELECT t1."date"
    ,t1."rank"
    ,t1.song
    ,t1.artist
    ,t1."last-week"
    ,t1."peak-rank"
    ,t1."weeks-on-board"
FROM PUBLIC."Billboard" AS t1 LIMIT 100


SELECT 
    t1.song
    ,t1.artist
FROM PUBLIC."Billboard" AS t1
WHERE 
    t1.artist= 'Chuck Berry'
    
SELECT 
    t1.artist
    ,t1.song
    ,count(*) AS "#song"
FROM
    PUBLIC."Billboard" AS t1
--WHERE 
    --t1.artist = 'Chuck Berry' OR t1.artist = 'Frankie Vaughan'
--    t1.artist IN( 'Chuck Berry', 'Frankie Vaughan', 'Ed Sheeran')
GROUP BY 
    t1.artist
    ,t1.song
ORDER BY "#song" DESC 