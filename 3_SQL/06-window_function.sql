WITH cte_billboard AS (
  SELECT
    t1.artist,
    t1.song,
    row_number() over(
      ORDER BY
        artist,
        song
    ) as "row_number",
    row_number() over(
      partition by artist
      ORDER BY
        artist,
        song
    ) as "row_number_artist"
  FROM
    PUBLIC."Billboard" AS t1
  ORDER BY
    t1.artist,
    t1.song
)
select
  *
from
  cte_billboard
WHERE
  "row_number_artist" = 1;

WITH cte_billboard AS (
  SELECT
    DISTINCT t1.artist,
    t1.song
  FROM
    PUBLIC."Billboard" AS t1
  ORDER BY
    t1.artist,
    t1.song
)
select
  *,
  row_number() over(
    ORDER BY
      artist,
      song
  ) as "row_number",
  row_number() over(
    partition by artist
    ORDER BY
      artist,
      song
  ) as "row_number_artist",
  rank() over(
    PARTITION BY artist
    ORDER BY
      artist,
      song
  ) AS "rank",
  lag(song, 1) over(
    PARTITION BY artist
    ORDER BY
      artist,
      song
  ) AS "lag_song",
  lead(song, 1) over(
    PARTITION BY artist
    ORDER BY
      artist,
      song
  ) AS "lead_song",
  first_value(song) over(
    PARTITION BY artist
    ORDER BY
      artist,
      song
  ) AS "first_song",
  last_value(song) over(
    PARTITION BY artist
    ORDER BY
      artist,
      song RANGE BETWEEN UNBOUNDED PRECEDING
      AND UNBOUNDED FOLLOWING
  ) AS "last_song"
from
  cte_billboard;