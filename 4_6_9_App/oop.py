import datetime
import math

class Person:
    def __init__(self, name:str, last_name: str, date_of_birth: datetime.date) -> None:
        self.date_of_birth = date_of_birth
        self.last_name = last_name
        self.name = name

    @property
    def age(self) -> int:
        return math.floor((datetime.date.today() - self.date_of_birth).days / 365.2425)

    def __str__(self)->str:
        return f"{self.name} {self.last_name} is {self.age} years old"

ze = Person(name='Ze', last_name='da Silva', date_of_birth=datetime.date(1997,6,16))
print(ze)
print(ze.name)
print(ze.last_name)
print(ze.age)