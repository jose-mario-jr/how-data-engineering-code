# %%
from os import sep
import requests
from bs4 import BeautifulSoup as bs
import pandas as pd
# %%
url = 'https://www.vivareal.com.br/venda/minas-gerais/uberaba/apartamento_residencial/?pagina={}'

# %%

i = 1
res = requests.get(url)
soup = bs(res.text)


# %%
houses = soup.find_all(
    'a', {'class': 'property-card__content-link js-card-title'})
qtd_houses = float(soup.find(
    'strong', {'class': 'results-summary__count js-total-records'}).text.replace('.', ''))
# %%
houses
# %%
len(houses)
# %%
qtd_houses
# %%
house = houses[0]
# %%
df = pd.DataFrame(columns=[
    'description',
    'address',
    'area',
    'rooms',
    'toilets',
    'garages',
    'price',
    'condo',
    'wlink',
])
i = 0

res = requests.get(url.format(i))
soup = bs(res.text)
qtd_houses = float(soup.find(
    'strong', {'class': 'results-summary__count js-total-records'}).text.replace('.', ''))

# %%

while qtd_houses > df.shape[0]:
    i += 1
    print(f"valor i: {i} \t\t qtd_imoveis: {df.shape[0]}")
    res = requests.get(url.format(i))
    soup = bs(res.text)
    houses = soup.find_all(
        'a', {'class': 'property-card__content-link js-card-title'})
    for house in houses:
        try:
            description = house.find(
                'span', {'class': 'property-card__title'}).text.strip()
        except:
            description = None
        try:
            address = house.find(
                'span', {'class': 'property-card__address'}).text.strip()
        except:
            address = None
        try:
            area = house.find(
                'span', {'class': 'js-property-card-detail-area'}).text.strip()
        except:
            area = None
        try:
            rooms = house.find(
                'li', {'class': 'property-card__detail-room'}).span.text.strip()
        except:
            rooms = None
        try:
            toilets = house.find(
                'li', {'class': 'property-card__detail-bathroom'}).span.text.strip()
        except:
            toilets = None
        try:
            garages = house.find(
                'li', {'class': 'property-card__detail-garage'}).span.text.strip()
        except:
            garages = None
        try:
            price = house.find(
                'div', {'class': 'property-card__price'}).p.text.strip()
        except:
            price = None
        try:
            condo = house.find(
                'strong', {'class': 'js-condo-price'}).text.strip()
        except:
            condo = None
        try:
            wlink = 'https://www.vivareal.com.br' + house['href']
        except:
            wlink = None

        df.loc[df.shape[0]] = [
            description,
            address,
            area,
            rooms,
            toilets,
            garages,
            price,
            condo,
            wlink,
        ]

# %%
df
# %%
df.to_csv('banco_de_imoveis.csv', sep=';', index=False)