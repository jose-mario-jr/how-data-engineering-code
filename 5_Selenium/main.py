# %%
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
import sys


cep = sys.argv[1]

if cep:
    s = Service('.\src\chromedriver.exe')
    driver = webdriver.Chrome(service=s)

    driver.get('https://buscacepinter.correios.com.br/app/endereco/index.php?t')
    elem_cep = driver.find_element(By.NAME, 'endereco')
    elem_cmb = driver.find_element(By.NAME, 'tipoCEP')

    elem_cep.clear()
    elem_cep.send_keys('38073750')
    elem_cmb.click()
    driver.find_element(
        By.XPATH, '//*[@id="formulario"]/div[2]/div/div[2]/select/option[6]').click()
    driver.find_element(By.ID, 'btn_pesquisar').click()

    driver.implicitly_wait(5)
    logradouro = driver.find_element(
        By.XPATH, '//*[@id="resultado-DNEC"]/tbody/tr/td[1]').text
    bairro = driver.find_element(
        By.XPATH, '//*[@id="resultado-DNEC"]/tbody/tr/td[2]').text
    localidade = driver.find_element(
        By.XPATH, '//*[@id="resultado-DNEC"]/tbody/tr/td[3]').text

    driver.close()

    print("""
    Para CEP {}, temos
    Endereço: {}
    Bairro: {}
    Localidade: {}
    """.format(cep, logradouro, bairro, localidade))


# %%
# entra no site
# driver.get('https://howedu.com.br/')
# aceita cookies
# driver.find_element(By.XPATH,
# '/html/body/div[2]/div[2]/div/div[2]/button[2]').click()
# clica em bootcamps
# driver.find_element(By.XPATH,
# '//*[@id="post-37"]/div/div/div/section[1]/div/div[1]/div/div[3]/div/div/a').click()
