# Git part of the course

- I already know, but wanted to do the part of it anyways

# Chapters

- I'll already put the titles and some annotations here

## 1. Init

- $ git init
  or
- initialize in vscode
  or
- initialize directly in github in vscode

## 2. Commit

- Started the readme template untill "Facilitadores"
- $ git add .
  or
- $ git add README.md
- git commit
  or
- git commit -m "Initial commit"
- git status
- git log

## .gitignore

- created .gitignore and .todo files
- filled .todo file and explained how to use it
- gitignore.io by toptal, filled with
  - python;
  - jupyternotebooks;
  - visualstudiocode;
  - pycharm;
  - virtualenv;
  - venv
- also added this code to .gitignore

```
### Personal
.env
.todo
```

- it will be commented so the .todo goes to the repo

## Git Checkout - Merge and rebase

- $ git checkout -b features1
- $ git branch
  show the available branches with the one you're on is pointed with an \*
- the professor shows a way to build a branch and do checkout over and over, showing it disappears and checking the branch out again and showing the data.
- I was doing it to do merge and rebase, ended up naming the branch feature2, not good, because it is needed to follow the naming convention
- $ git rebase target_branch
  it rebases the commits, in other words: the commits that were once from the other branch, now belongs to the current branch. It is not very good practice because it changes the history.
- also showed the git graph extension, that is very good

## Github

- Shows the creating of the project
- The licensing part have a questionnaire about preferences about project, to see what license is good for the project
- shows the commands, cline with https
- show how to do the remote add origin
  - and the git branch -M main rename master to main
  - after do the push
- pull request settings on github, should be a lot of stuff

## Operations mentioned and made in repo

- git pull and push;
- pull request;
- git tag;
- conflicts.
# Template is starting from here:

# HOW Bootcamp Formação de dados

## Engenharia de dados - jun/21

repositório de códigos do bootcamp de ED de Jun/21

## Facilitadores

- André Sionek - [Github](https://github.com/andresionek91)
- Rhuan Lima - [Github](https://github.com/rhuanlima)

## Calendário:

- 10/06 (Quinta): Encontro inaugural
- 17/06 (Quinta): Live com convidado
- 24/06 (Quinta): Prática e Cases: Capturando dados + testes + jenkins
- 29/06 (Terça): Live com convidado
- 01/07 (Quinta): Prática e Cases
- 06/07 (Terça): Group Programming
- 08/07 (Quinta): Prática e Cases: AWS Lambda
- 13/07 (Terça): Group Programming
- 15/07 (Quinta): Prática e Cases: Ingestão de dados
- 20/07 (Terça): Group Programming
- 22/07 (Quinta): Prática e Cases: Mensageria
- 27/07 (Terça): Group Programming
- 29/07 (Quinta): Prática e Cases: Apache Spark
- 03/08 (Terça): Group Programming
- 05/08 (Quinta): Live - De engenheiro a streamer de dados
- 12/08 (Quinta): Encerramento
